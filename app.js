var hangman = (function() {

  var secretsList = ['hello', 'privet', 'javascript'];
  var attempt = [];
  var hit = [];
  var secret = '';
  var count = 0;

  return {
    init: function() {
      secret = this.createSecter();
      this.show();
      this.handleInput();
    },

    createSecter: function() {
      var min = 0;
      var max = secretsList.length;
      var rand = Math.floor(Math.random() * (max - min) + min);
      var secret = secretsList[rand].split('');
      hit.length = secret.length;
      console.log(secret); // + test
      return secret;
    },

    show: function() {
      var divShow = document.querySelector('.show');
      var divAttempt = document.querySelector('.attempt');
      var output = '';
      for (var i = 0; i < secret.length; i++) {
        if (secret[i] == hit[i]) {
          output += secret[i] + '';
        } else {
          output += '_ ';
        }
      }
      divShow.innerHTML = output;
      output = '';

      for (var i = 0; i < attempt.length; i++) {
        output += attempt[i];
      }
      divAttempt.innerHTML = output;
      output = '';

      if(this.winner()) {
        this.showResult(true);
      }
    },

    handleInput: function() {
      var form = document.forms['hangman-form'];
      var self = this;
      form.addEventListener('submit', function handleSubmit(e) {
        e.preventDefault();
        var textInput = this.querySelector('input[type="text"]');
        var simbol = textInput.value;
        if ( (simbol == '') || (simbol.length > 1) || (typeof simbol == 'number') ) {
          return;
        } else {
          simbol = simbol.toLowerCase();
          var flag = false;
          for (var i = 0; i < secret.length; i++) {
            if (simbol == secret[i]) {
              self.addHit(simbol, i);
              flag = true;
            }
          }
          if (!flag) {
            self.addAttempt(simbol);
          }
          textInput.value = '';
        }
        if ( (self.winner()) || (self.loss()) ) {
          this.removeEventListener('submit', handleSubmit);
          this.classList.add('hide');
        }
      });
    },

    addHit: function(simbol, pos) {
      hit[pos] = simbol;
      this.show();
    },

    addAttempt: function(simbol) {
      var img = document.querySelector('.game img');
      var src = img.getAttribute('src');
      attempt.push(simbol);
      count++;
      src = src.substr(0, src.length - 5) + count + '.png'
      img.src = src;

      if (count == 4) {
        this.showResult(false);
        return;
      }
      this.show();
    },

    winner: function() {
      var flag = false;
      for (var i = 0; i < secret.length; i++) {
        if (hit.indexOf(secret[i]) != -1) {
          flag = true;
        } else {
          flag = false;
          break;
        }
      }
      return flag;
    },

    loss: function() {
      if (count == 4) {
        return true;
      }
    },

    showResult: function(flag) {
      var winner = document.querySelector('.winner');
      var secretShow = secret.join('');

      if (flag) {
        winner.innerHTML = 'Угадал! Слово было - ' + secretShow;
      } else {
        winner.innerHTML = 'Проиграл! Слово было - ' + secretShow;
      }

      this.destroy();
    },

    destroy: function() {
      var btn = document.querySelector('button');
      var self = this;

      btn.classList.add('active');
      btn.addEventListener('click', function() {
        document.querySelector('.winner').innerHTML = '';
        document.forms['hangman-form'].classList.remove('hide');
        document.querySelector('.game img').src = 'img/0.png';
        attempt = [];
        hit = [];
        secret = '';
        count = 0;
        this.classList.remove('active');
        self.init();
      });
    }
  };

})();

hangman.init();